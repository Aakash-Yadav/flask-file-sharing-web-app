**using Flaks and Python3 http.server created a clone of shair-it app**

### requrement:
- flask 
- bs4
- requests 
- ifcfg
##### command
1. pip3 install Flask
2. pip3 install bs4
3. pip3 install requests
4. pip3 install ifcfg

### What is Flask Python:
_Flask is a web framework, it’s a Python module that lets you develop web applications easily. It’s has a small and easy-to-extend core: it’s a microframework that doesn’t include an ORM (Object Relational Manager) or such features._

### What is Flask?
Flask is a web application framework written in Python. It was developed by Armin Ronacher, who led a team of international Python enthusiasts called Poocco. Flask is based on the Werkzeg WSGI toolkit and the Jinja2 template engine.Both are Pocco projects.

#### Python3 server:
Python standard library comes with a in-built webserver which can be invoked for simple web client server communication. The port number can be assigned programmatically and the web server is accessed through this port. Though it is not a full featured web server which can parse many kinds of file, it can parse simple static html files and serve them by responding them with required response codes.

#### RUN WEB APP:
##### First  command
`python3 -m http.server 4000`
![alt text](https://xp.io/storage/29xLniby.png)

### second run python main app:
##### command
`python3 main.py`
![alt text](https://xp.io/storage/29xTR9pA.png)
_select operating system index [num]_

#### Flask server start:
_It will generat 3 file (PATH.txt Folder_loc.json and File_loc.json) contain all data related system file_
![alt text](https://xp.io/storage/29y1iQ2V.png)

#### HOME PAGE :
![alt text](https://xp.io/storage/29ygAw7Z.png)
1. send files
2. receive files

#### Menu page:
![alt text](https://xp.io/storage/29ynAt7l.png)
#### select file or folder 
![alt text](https://xp.io/storage/29yt6Uiw.png)
![alt text](https://xp.io/storage/29yvPjTu.png)

#### select file 
![alt text](https://xp.io/storage/29yBXE71.png)

#### GET QRCODE:
![alt text](https://xp.io/storage/29yEPLSX.png)

#### Note & Conclusion
1. start python server at 4000 (python3 -m http.server 4000)
2. Be on same wifi or hotspot

#### thank you 











