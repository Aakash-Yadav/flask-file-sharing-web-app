from flask import Flask ,session ,render_template,request,session,url_for,redirect 
from os import urandom 
from config import we_scraping  ,run ,retrun_local_ip,all_path
import json 
import urllib.parse

run()

    
with open('Folder_loc.json') as f:
    all_folder_name_loc = json.loads(f.read())
    
with open('File_loc.json') as f:
    all_file_name_loc = json.loads(f.read())  


app = Flask(__name__)

server_location =(f'http://{retrun_local_ip()}:4000/')

app.config['SECRET_KEY'] = """%s--%s--%s"""%(urandom(50),urandom(50),urandom(50))

@app.route('/')
def index():
    return render_template('Home.html')

@app.route('/upload',methods=['GET',"POST"])
def Upload():
    
    folder_name , file_name = we_scraping(server_location)
    
    if request.method == 'POST':
        
        fold_name=(request.form.get('name'))
        FileName = (request.form.get('name2'))
        
        if fold_name:
            new = all_folder_name_loc.get(fold_name[0:-1]) 
            new = server_location + new.replace(f'{all_path}','')[1:]

            fo_name , fi_name = we_scraping(new)
            return render_template('upload2.html',folder_name=fo_name,file_name=fi_name)
        
        elif FileName:
            encodedStr  = FileName
            print(FileName)
            FileName = urllib.parse.unquote(encodedStr)
            
            file_name_data = all_file_name_loc.get(FileName)
            v=urllib.parse.quote(file_name_data.replace(f'{all_path}','')[1:])
            file_name_data_= server_location + v
            return render_template('last_page.html',x_val=(file_name_data_))

            
    return render_template('upload2.html',folder_name=folder_name,file_name=file_name)



if __name__ == '__main__':
    run()
    app.run(debug=1)